param([string] $domain, [string[]] $alternativeDomains, [string] $certPassword, [string] $godaddyRootDomain, [string] $godaddyKey, [string] $godaddySecret, [string] $outputPath)

if([String]::IsNullOrEmpty($outputPath))
{
  $outputPath = $pwd
}


$certRef = "cert-$([Guid]::NewGuid().ToString())"
$handlerProfileRef = "manual-dns-json"

$domainsToValidate = New-Object System.Collections.Generic.List[System.Object]

$mainDomain = @{domain = $domain; ref = "dns-$([Guid]::NewGuid().ToString())"}

$alternates = @()

if($alternativeDomains)
{
  $alternates = $alternativeDomains | % { @{domain = $_; ref = "dns-$([Guid]::NewGuid().ToString())"} }
}

$domainsToValidate.Add($mainDomain)
foreach($alt in $alternates)
{
  $domainsToValidate.Add($alt)
}

Set-ACMEChallengeHandlerProfile -ProfileName $handlerProfileRef -ChallengeType "dns-01" -Handler manual -HandlerParameters @{ OutputJson = $true }

#validate all domains, generate cert for $domain with $alternativeDomains as subject alternative names
Foreach ($dom in $domainsToValidate)
{
  $validateDomain = $dom.domain
  $identifierRef = $dom.ref

  write-output "`n`n=================================================================="
  write-output "validating domain for $validateDomain with identifierRef $identifierRef"

  $acmeidentifier = New-ACMEIdentifier -dns $validateDomain -alias $identifierRef

  if($acmeidentifier.Status -eq "valid") {
    continue;
  }

  write-output "`nCompleting Acme Challenge"
  $challenge = (Complete-ACMEChallenge -IdentifierRef $identifierRef -HandlerProfileRef $handlerProfileRef).Challenges | ? { $_.Type -eq "dns-01" }

  # | select -Expand DnsDetails | select RRValue

  # extract the resource record data from $challenge
  $recordName = $challenge.Challenge.RecordName
  $recordValue = $challenge.Challenge.RecordValue

  # godaddy api to write the resource record
  # put https://api.godaddy.com/v1/domains/$domain/records/TXT/$resourceName
  # data [{"data": "$resourceData", "ttl": 600}]
  $headers = @{authorization = "sso-key ${godaddyKey}:${godaddySecret}"}
  $body = "[{`"data`": `"$recordValue`", `"ttl`": 600}]"

  write-output "`n`nAdding TXT record to $domain for $recordName - $recordValue"
  invoke-webrequest "https://api.godaddy.com/v1/domains/$godaddyRootDomain/records/TXT/$recordName" -method Put -Body $body -ContentType "application/json" -Headers $headers

  Write-Output "Waiting after updating dns record"
  start-sleep 60

  write-output "`n`nSubmitting Acme Challenge"
  Submit-ACMEChallenge -IdentifierRef $identifierRef -ChallengeType "dns-01"

  do
  {
    start-sleep 10
    $status = $(Update-AcmeIdentifier -IdentifierRef $identifierRef).Status
  } until ($status -ne "pending")

  # check that it's valid
  if($status -ne "valid")
  {
    throw "Validation failed for $validateDomain - $identifierRef!"
  }
}

write-output "`n`nGenerating Acme certificate`n`n"

$alternativeIdentifierRefs = @()
if($alternativeDomains)
{
  $alternativeIdentifierRefs = $alternates | % { $_.ref }
  New-ACMECertificate -Generate -IdentifierRef $mainDomain.ref -AlternativeIdentifierRefs $alternativeIdentifierRefs -Alias $certRef
}
else
{
  New-ACMECertificate -Generate -IdentifierRef $mainDomain.ref -Alias $certRef
}


write-output "`n`nSubmitting Acme certificate"
Submit-ACMECertificate -CertificateRef $certRef

do
{
  start-sleep 10
  $issuerSerialNumber = $(Update-ACMECertificate -CertificateRef $certRef).IssuerSerialNumber
} until (![String]::IsNullOrEmpty($issuerSerialNumber))

write-output "`n`nExporting certificate files"
Get-ACMECertificate $certRef -ExportKeyPEM "$([System.IO.Path]::Combine($outputPath, $domain + '.key.pem'))"
Get-ACMECertificate $certRef -ExportCertificatePEM "$([System.IO.Path]::Combine($outputPath, $domain + '.crt.pem'))"
Get-ACMECertificate $certRef -ExportIssuerPEM "$([System.IO.Path]::Combine($outputPath, $domain + '-issuer.crt.pem'))"
Get-ACMECertificate $certRef -ExportPkcs12 "$([System.IO.Path]::Combine($outputPath, $domain + '.pfx'))" -CertificatePassword "$certPassword"

Get-Content "$([System.IO.Path]::Combine($outputPath, $domain + '.crt.pem'))" > "$([System.IO.Path]::Combine($outputPath, $domain + '.crt.chain.pem'))"
Get-Content "$([System.IO.Path]::Combine($outputPath, $domain + '-issuer.crt.pem'))" >> "$([System.IO.Path]::Combine($outputPath, $domain + '.crt.chain.pem'))"