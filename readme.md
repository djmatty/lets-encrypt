#Automates Lets-Encrypt using ACMESharp and GoDaddy api

##Mail Certificate
.\get-new-cert.ps1 -domain "mail.djmatty.com" -certPassword "Password123" -godaddyRootDomain "djmatty.com" -godaddyKey "godaddykey" -godaddySecret "godaddysecret" -outputPath "c:\0source"

##Multi Certificate
.\get-new-cert.ps1 -domain "djmatty.com" -alternativeDomains @("cp.djmatty.com", "dl.djmatty.com", "snr.djmatty.com", "whs.djmatty.com") -certPassword "Password123" -godaddyRootDomain "djmatty.com" -godaddyKey "godaddykey" -godaddySecret "godaddysecret" -outputPath "c:\0source"
